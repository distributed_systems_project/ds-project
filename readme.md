Distributed Trigger Counter
===========================
The trigger counter simulation engine is designed to test the performance of various trigger counter algorithms and different implementations of the same algorithm.  There is a single debug-master that initiates the simulation and collects results after the simulation is complete. There are a number of different implementations included in folders that follow the triggercounter-template. 

debug-master
-------------
The debug-master lives outside the algorithm and performs the following functions:
* Launches N process nodes at the start of the simulation.
* Sends out W triggers to randomly selected triggercounter nodes during the simulation.
* Queries and collects the following information from N process nodes at the end of the simulation.
	* number of triggers counted by each node
	* number of messages sent by each node
	* number of messages received by each node
* Generates a report containing the following:
	* message complexity (total number of messages transferred for N nodes to count W triggers)
	* maxmsgload (the maximum number of messages sent or received by any given node)
	* any anomolies encountered (e.g. #sent != #received, triggercount != W, etc.)

triggercounter-template
----------------------------------------
This is the base template on which all the triggercounter implementations are based.  It defines the basic structure and the required command line parameter parsing so that the debug-master would be able to launch N processes. 
	
triggercounter-algorithm-implementation
----------------------------------------
Each implementation will reside in a folder with the algorithm type and the implementation name encoded in the folder name.  These two parameters will also be encoded in the underlying class name.  The debug-master will maintain a list of implementations and will instantiate N nodes of this type using the matching folder and class name.  

triggercounter-trivial-centralized
----------------------------------------
This is the centralized implementation (or trivial algorithm case) which is used to test out the simulation engine's functionality.  

more algorithms and implementations to be added here...
----------------------------------------