import java.lang.Math;
import java.util.Set;

public class TcLayeredNode implements TcAlgorithm {
 
  private int Cx=0; // my current count
  private int Ct=0; // my total triggers received
  private int Ct_early=0; //triggers received before start
  private int Ct_late=0; //triggers received after termination
  private double tau; // my threshold to send coin
  private double tau_1; // threshold at lower layer
  private int omega_hat; // remaining triggers this round
  private boolean omega_prime_wait=false; // received omega_prime from one child
  private int level; // my level in the tree
  private int n; // number of processes
  private ParentNodes parents; // parent nodes
  private ChildNodes children; // parent nodes
  private int myParentId; // ID of my parent in binary tree structure
  private boolean isRoot=false; // I am root
  private boolean isLeaf=false; // I am a leaf node
  private int parentMethod;
  private int lastParent = 0; // For round-robin
  private int id;
  private boolean algorithmStarted=false;
  private boolean algorithmTerminated=false;
  

  static final int RANDOMIZED = 0;
  static final int ROUNDROBIN = 1;
  
  public TcLayeredNode(int omega, int level, ParentNodes pn, ChildNodes cn, int parentMethod, int n, int id) {
    this.omega_hat = omega;
    this.parents = pn;
    this.children = cn;
    this.level = level;
    this.parentMethod = parentMethod;
    this.n = n;
    this.isRoot=pn.isRoot();
    this.isLeaf=cn.isLeaf();
    this.id = id;
  }
  
  private void startRound() {
    if (isRoot) {
      receiveNewRound(omega_hat);
    }
  }
  
  private void receiveNewRound(int omega_hat) {
    algorithmStarted = true;
    this.omega_hat = omega_hat;
    computeThreshold(omega_hat);
    if (!isLeaf) {
    	sendNewRound(omega_hat);
    }
  }
  
  private void computeThreshold(int omega_hat) {
	  System.out.printf("[tcAlgorithm]<%d>in computeThreshold omega_hat:%d tau:%f%n", id, omega_hat, tau);
	  
	  if (id == 1 && Cx > 0)
		  System.out.print("root");
	  
    tau = ((double)omega_hat)/((double)(4.0*Math.pow(2.0,(double)level))*Math.log10((double)n+1.0));
    System.out.printf("[tcAlgorithm]<%d>after computeThreshold omega_hat:%d tau:%f%n", id, omega_hat, tau);
    
    if (!isLeaf) {
      tau_1 = ((double)omega_hat)/((double)(4.0*Math.pow(2.0,(double)(level+1)))*Math.log10((double)n+1.0));
    }
  }
  
  public void receiveTrigger() {
    if (algorithmStarted && !algorithmTerminated) {
    	Ct += 1;
    	Cx += 1;
    	checkThreshold();
    } else if (!algorithmStarted) {
      Ct_early += 1;
    } else if (algorithmTerminated) {
      Ct_late += 1;
    }
  }
  
  private void receiveCoin() {
    if (!isLeaf) {
      Cx += Math.ceil(tau_1);
      checkThreshold();
    }
  }
  
  private void receiveEOR(int parentId) {
    // End of Round message received from parent
    this.myParentId = parentId;
    if (!isLeaf) {
      sendEOR(id);
    } else {
      sendOmegaPrime(Cx);
      Cx = 0;
    }
  }
  
  private void sendEOR(int myId) {
    sendToChildren("EOR " + myId);
  }
  
  private void sendOmegaPrime(int omega_prime) {
    sendToMyParent("omegaPrime " + Integer.toString(omega_prime));
  }
  
  private void receiveOmegaPrime(int omega_prime) {
	  
	  System.out.println("omegaprime");
	  
	  System.out.printf("[tcAlgorithm]<%d> omega prime:%d%n", id, omega_prime);
	  
    Cx += omega_prime;
    
    if (!omega_prime_wait) {
      // This is the first omega_prime message received from a child
      // In a binary tree, expect omega_prime messages from two children
      omega_prime_wait = true;
    } else {
      // This is the second omega_prime message received.
      // Proceed with sending omega_prime to parent.
      omega_prime_wait = false;
      if (!isRoot) {
        sendOmegaPrime(Cx);
        Cx = 0;
      } else {
        omega_hat -= Cx;
        Cx = 0;
        if (omega_hat>0) {
          computeThreshold(omega_hat);
          sendNewRound(omega_hat);
          System.out.printf("[tcAlgorithm]<" + id + "> Starting new round with omega_hat="+Integer.toString(omega_hat));
        } else {
          algorithmTerminated = true;
          sendTerminate();
          System.out.printf("[tcAlgorithm]<\" + id + \"> All triggers received");
        }
      }
    }
  }
  
  private void sendTerminate() {
    sendToChildren("terminate");
  }
  
  private void receiveTerminate() {
    algorithmTerminated = true;
    if (!isLeaf) {
      sendTerminate();
    }
  }
  
  private void sendNewRound(int omega_hat) {
    sendToChildren("newRound " + Integer.toString(omega_hat));
  }
  
  private void sendCoin() {
    sendToAnyParent("coin");
  }
  
  private int chooseParent() {
    // Choose a parent node to send to
    int parentIndex;
    int[] nodeNums = parents.getNodes();
    if (parentMethod == RANDOMIZED) {
      parentIndex = (int)(nodeNums.length * Math.random());
    } else {
      // Round-robin
      parentIndex = (lastParent+1)%(nodeNums.length);
    }
    return nodeNums[parentIndex];
  }
  
  private void checkThreshold() {

    //if(id == 1) {
      System.out.printf("[tcAlgorithm]<" + id + "> " +
              "Cx=" + Integer.toString(Cx) +
              ", tau=" + Double.toString(tau) +
              ", level=" + Integer.toString(level) +
              "%n");
    //}

    if(id == 1 && Cx > 50)
      System.out.println("break");

    if (isRoot) {
      if (Cx >= omega_hat/2) {
        // Root sends End of Round to children
        sendEOR(id);
      }
    } else {
      if (Cx >= tau) {
        // Non-root sends coin to parent
        // Subtract Math.ceil(tau) from Cx to avoid double-counting
        Cx -= Math.ceil(tau);
        sendCoin();
      }
	 }
  }
  
  private void sendToAnyParent(String message) {
    if (!isRoot) {
    	int parent = chooseParent();
    	parents.sendToOneParent(parent, message);   
    }
  }
  
  private void sendToMyParent(String message) {
    if (!isRoot) {
    	parents.sendToOneParent(myParentId, message);   
    }
  }
  
  private void sendToChildren(String message) {
    if (!isLeaf) {
      children.sendToChildren(message);
	 }
  }
  
  public int reportTotalTriggers() {
    return Ct;
  }
  
  public int reportEarlyTriggers() {
    return Ct_early;
  }
  
  public int reportLateTriggers() {
    return Ct_late;
  }
  
  public boolean reportTerminated() {
    return algorithmTerminated;
  }
  
  public synchronized void receiveMessage(String message) {
	  if (Symbols.LOGGING >= 3 )
		  System.out.printf("[tcAlgorithm]<%d>receiveMessage:%s\n", id, message);
    String[] msgPart = message.split(" ");
    
    switch(msgPart[0]) {
      case "newRound":
  			receiveNewRound(Integer.parseInt(msgPart[1]));
      	break;
      case "coin":
      	receiveCoin();
      	break;
      case "EOR":
      	receiveEOR(Integer.parseInt(msgPart[1]));
      	break;
      case "omegaPrime":
      	receiveOmegaPrime(Integer.parseInt(msgPart[1]));
      	break;
      case "terminate":
      	receiveTerminate();
      	break;
      case "trigger":
      	receiveTrigger();
      	break;
      case "start":
      	startRound();
      	break;
      default:
      	System.out.printf("[tcAlgorithm]<%d> received bad message: %s %n", id, message);
    }
    
  }
}