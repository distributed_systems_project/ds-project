import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ParentNodes {

	private HashMap<Integer, InetSocketAddress> parentIpAddress;
	private Lock lock = new ReentrantLock();
	public int nNodeNum;
	private int nSentMessages = 0;

	public ParentNodes (int nodeNum) {		
		parentIpAddress = new HashMap<Integer, InetSocketAddress> ();
		this.nNodeNum = nodeNum;
		if (Symbols.LOGGING >= 5 )
			System.out.printf("[ParentNodes.java]<%d>ParentNodes class instantiated\n", this.nNodeNum);		
	}

	public void addParentNode (int nNodeNum, int nPort) {
		addParentNode(nNodeNum, "localhost", nPort);
	}

	public void addParentNode (int nNodeNum, String hostName, int nPort) {

		InetSocketAddress inetSock = new InetSocketAddress(hostName, nPort);		
		addParentNode(nNodeNum, inetSock);
	}

	public void addParentNode (int nNodeNum, InetSocketAddress inetSock) {
		try {
			lock.lock();
			parentIpAddress.put(nNodeNum, inetSock);
			if (Symbols.LOGGING >= 3 )
				System.out.printf("[ParentNodes.java]<%d>Adding parent node %d:%s\n", this.nNodeNum, nNodeNum, inetSock);	
		}
		finally {
			lock.unlock();
		}
	}
	
	public int getSentMessages() {
		return nSentMessages;
	}

	public void removeParentNode (int nNodeNum) {
		try {
			lock.lock();
			parentIpAddress.remove(nNodeNum);
		}
		finally {
			lock.unlock();
		}
	}

	public void clearAllParentNode (int nNodeNum) {
		try {
			lock.lock();
			parentIpAddress.clear();
		}
		finally {
			lock.unlock();
		}
	}

	public int [] getNodes () {
		
		int index = 0;
		int[] nodeNums = new int[parentIpAddress.keySet().size()];		
		Object[] keys = parentIpAddress.keySet().toArray();
		
		Arrays.sort(keys);
		
		for(Object key : keys) {
			nodeNums[index++] = (Integer) key;
		}

//		for(Integer i : parentIpAddress.keySet()) {
//		  nodeNums[index++] = i; //note the autounboxing here
//		}
//		return parentIpAddress.keySet();
		  return nodeNums;
	}

	public Collection<InetSocketAddress> getValues () {
		return parentIpAddress.values();
	}

	public boolean isRoot() {
		boolean bRoot = true;
		if (!parentIpAddress.isEmpty())
			bRoot = false;
		return bRoot;
	}

	public String sendToAllParents(String command) {

		StringBuilder stbResponse = new StringBuilder();

		for ( InetSocketAddress address : parentIpAddress.values()) {
			stbResponse.append("Notifying:" + address.toString() + " ");

			if (Symbols.LOGGING >= 3 )
				System.out.printf("[ParentNodes.java]<%d>sendToAllParents Parent:%s Cmd:%s\n", this.nNodeNum, address.toString(), command);
			nSentMessages++;

			try (Socket tcpSocket = new Socket(address.getHostName(), address.getPort());
					PrintWriter tcpOut = new PrintWriter(tcpSocket.getOutputStream(), true);
					BufferedReader tcpIn = new BufferedReader(new InputStreamReader(tcpSocket.getInputStream()));
					)
					{
				// Send command to server
				tcpOut.println(command);

				// Display response from server
				if (Symbols.LOGGING >= 3 )
					System.out.printf("[ParentNodes.java]<%d> sendToAllParents response to server:%s\n", this.nNodeNum, tcpIn.readLine());
					}
			catch (IOException e) {
				if (Symbols.LOGGING >= 3 )
					e.printStackTrace();
			}
			finally {}
		}

		return stbResponse.toString();
	}

	public String sendToOneParent(int nodeNum, String command) {
		
		StringBuilder stbResponse = new StringBuilder();

		InetSocketAddress address = parentIpAddress.get(nodeNum);
		
		if (address == null){
			return "The node number is not a parent of this node";
		}
		
		stbResponse.append("Notifying:" + address.toString() + " ");

		if (Symbols.LOGGING >= 3 )
			System.out.printf("[ParentNodes.java]<%d>sendToOneParent:%s Cmd:%s\n", this.nNodeNum, address.toString(), command);
		nSentMessages++;

		try (Socket tcpSocket = new Socket(address.getHostName(), address.getPort());
				PrintWriter tcpOut = new PrintWriter(tcpSocket.getOutputStream(), true);
				BufferedReader tcpIn = new BufferedReader(new InputStreamReader(tcpSocket.getInputStream()));
				)
				{
			// Send command to server
			tcpOut.println(command);

			// Display response from server
			if (Symbols.LOGGING >= 3 )
				System.out.printf("[ParentNodes.java]<%d> sendToOneParent response to server:%s\n", this.nNodeNum, "Not implemented");//, tcpIn.readLine());
				tcpOut.close();
				}
		catch (IOException e) {
			if (Symbols.LOGGING >= 3 )
				e.printStackTrace();
		}
		finally {}


		return stbResponse.toString();
	}
}
