import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;


public class Server implements Runnable {

	private int nNodeNumber = 0;
	private int nPort = 0;
//	private HandleRequest handleRequest;
	private TcAlgorithm tcAlgorithm;
	public int nSentMessages = 0;
	public int nRecvMessages = 0;
	private ParentNodes parentNodes;
	private ChildNodes childNodes;

	public Server (int nNodeNum, int nPort, ParentNodes parNodes, ChildNodes chiNodes, TcAlgorithm tcAlgorithm) {
		this.nNodeNumber = nNodeNum;
		this.nPort = nPort;
//		this.handleRequest = new HandleRequest(this.nNodeNumber, parNodes, chiNodes);
		this.tcAlgorithm = tcAlgorithm;
		this.parentNodes = parNodes;
		this.childNodes = chiNodes;
	}
	
	public int getSentMessages() {
		nSentMessages = this.parentNodes.getSentMessages() + this.childNodes.getSentMessages();
		return nSentMessages;
	}
	
	public int getRecvMessages() {
		return nRecvMessages;
	}

	@Override
	public void run() {
		String response;

		ServerSocket listener;
		try {
			listener = new ServerSocket(nPort, 100);
			while(true)
			{
				Socket connectionSocket = listener.accept();
				BufferedReader inFromClient =
						new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
				DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
				String tag = inFromClient.readLine();
				nRecvMessages++;
				if (Symbols.LOGGING >= 3 )
					System.out.printf("[Server]<%d>Before tcAlgorithm.receiveMessage(tag)\n", nNodeNumber);
				tcAlgorithm.receiveMessage(tag);
				if (Symbols.LOGGING >= 3 )
					System.out.printf("[Server]<%d>After tcAlgorithm.receiveMessage(tag)\n", nNodeNumber);
				response = "Server received " + tag;
//				response = handleRequest.processRequest(tag);  //TODO
//				System.out.printf("[Server.java]<%d>client response:%s\n", nNodeNumber, response);

//				outToClient.writeBytes(response);
				outToClient.flush();
				connectionSocket.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			if (Symbols.LOGGING >= 3 )
				e.printStackTrace();
		}
	}
}
