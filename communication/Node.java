import java.net.InetSocketAddress;


public class Node {

	private ParentNodes parentNodes;
	private ChildNodes childNodes;
	private TcAlgorithm tcAlgorithm = null;
	private int nNodeNumber;
	private int nServerPort;
	private Server ipServer;
	public int nNodeLevel;
	public final static int PORT_OFFSET = 10000; 
	public final static int MAX_TRIGGER = 10;

	public Node (int nNodeNum, int nodeLevel) {
		this(nNodeNum, nodeLevel, nNodeNum + PORT_OFFSET);
	}

	public Node (int nNodeNum, int nodeLevel, int nPort) {
		this.nNodeNumber = nNodeNum;
		this.nServerPort = nPort;
		this.nNodeLevel = nodeLevel;
		
		parentNodes = new ParentNodes(this.nNodeNumber);
		childNodes = new ChildNodes(this.nNodeNumber);
	}

	public int getNodeLevel() {
		return nNodeLevel;
	}

    public void addAlgorithm(TcAlgorithm tca){
		Thread t1;
        tcAlgorithm = tca;
        
		if (Symbols.LOGGING >= 3 )
			System.out.printf("[Node.java]<%d> thread starting\n", nNodeNumber);
		this.ipServer = new Server (nNodeNumber, nServerPort, parentNodes, childNodes, tcAlgorithm);
		t1 = new Thread(this.ipServer);
		t1.start();
    }
    	
	public void addChildNode (int nNodeNum) {
		addChildNode(nNodeNum, "localhost", nNodeNum + PORT_OFFSET);
	}

	public void addChildNode (int nNodeNum, int nPort) {
		addChildNode(nNodeNum, "localhost", nPort);
	}

	public void addChildNode (int nNodeNum, String hostName, int nPort) {
		InetSocketAddress inetSock = new InetSocketAddress(hostName, nPort);		
		childNodes.addChildNode(nNodeNum, inetSock);
	}
	
	public void addParentNode (int nNodeNum) {
		addParentNode(nNodeNum, "localhost", nNodeNum + PORT_OFFSET);
	}

	public void addParentNode (int nNodeNum, int nPort) {
		addParentNode(nNodeNum, "localhost", nPort);
	}

	public void addParentNode (int nNodeNum, String hostName, int nPort) {
		InetSocketAddress inetSock = new InetSocketAddress(hostName, nPort);		
		parentNodes.addParentNode(nNodeNum, inetSock);
	}
	
	public TcAlgorithm getAlgorithm() {
		return this.tcAlgorithm;
	}
	
	public int getSentMessages() {
		return this.ipServer.getSentMessages();
	}
	
	public int getRecvMessages() {
		return this.ipServer.getRecvMessages();
	}

	
	public ParentNodes getParentNodes() {
		return parentNodes;
	}
	
	public ChildNodes getChildtNodes() {
		return childNodes;
	}	

	public static void main (String[] args) {

		int nNodeNum;
		int nLevelNum;
		int nParent;

		if (args.length < 2) {
			System.out.println("[Node.java]ERROR: Provide at least two arguements");
			System.out.println("[Node.java]\t(1) <N>: the node number for this item");
			System.out.println("[Node.java]\t(2) <N>: the level the node is on");
			System.out.printf("[Node.java]\t(3) (optional)<parent server id>: id of the parent server (assumes localhost w/ port %d + id)\n", PORT_OFFSET);

			System.exit(-1);
		}

		nNodeNum = Integer.parseInt(args[0]);
		nLevelNum = Integer.parseInt(args[1]);
		if (Symbols.LOGGING >= 3 )
			System.out.printf("[Node.java]nNodeNum:%d nLevelNum:%d\n", nNodeNum, nLevelNum);
		Node node = new Node(nNodeNum, nLevelNum);

		if (args.length > 2) {
			for (int i = 2; i < args.length; i++) {
				nParent = Integer.parseInt(args[i]);
				if (Symbols.LOGGING >= 3 )
					System.out.printf("[Node.java]nParent:%d\n", nParent);
				node.addParentNode(nParent, nParent + PORT_OFFSET);
			}
		}
	}
}
