import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class HandleRequest {  //TODO do I need this?

	int nNodeNumber;
	private ParentNodes parentNodes;
	private ChildNodes childNodes;
	private Lock lock = new ReentrantLock();
	private AtomicInteger nTriggerCount = new AtomicInteger(0);


	public HandleRequest(int nodeNum, ParentNodes parNodes, ChildNodes chiNodes) {

		this.nNodeNumber = nodeNum;
		this.parentNodes = parNodes;
		this.childNodes = chiNodes;
		System.out.printf("[HandleRequest.java]<%d> HandleRequest class instantiated\n", this.nNodeNumber);
	}

	public String processRequest(String request) {

		String response = "";
		boolean root = parentNodes.isRoot();

		System.out.printf("[HandleRequest.java]<%d>Request is:%s\n", this.nNodeNumber, request);
		String [] serverInput = request.split(" ");

		String keyWord = serverInput[0].trim();
		String data = "";

		if (serverInput.length > 1)
			data = serverInput[1].trim();

		if (root) {
			int count = nTriggerCount.getAndIncrement();
			System.out.printf("[HandleRequest.java]<%d>I am root count is:%d\n", this.nNodeNumber, count);
			response = String.format("I am root count is:%d", count);
		}
		else {

			switch(keyWord) {
			case "trigToParent": 
				response = trigToParent(data);
				break;
			case "trigToChild": 
				response = trigToChild(data);
				break;
			default: 
				response = "Invalid keyword";
			}
		}

		return response;

	}

	public String trigToParent(String data) {

		String strReason = null;
		int nTrigCount;

		//TODO call Jason's algorithm?

//		receiveTrigger();

		//		try {
		//			lock.lock();
		//
		//			nTrigCount = this.nTriggerCount.incrementAndGet();
		//
		//			if (nTrigCount >= Node.MAX_TRIGGER) {
		//				StringBuilder stbResponse = new StringBuilder("nTrigCount:" + nTrigCount + " ");
		//				stbResponse.append(parentNodes.sendToChildren("addToCount"));
		//
		//				this.nTriggerCount.set(0);
		//				strReason = stbResponse.toString();
		//			}
		//			else {
		//				strReason = String.format("No trigger, count is:%d", nTrigCount);
		//			}
		//		}
		//		finally {
		//			lock.unlock();
		//		}

		return strReason;
	}

	public String trigToChild(String data) {

		String strReason = null;
		int nTrigCount;

		//TODO call Jason's algorithm?



		//	try {
		//		lock.lock();
		//
		//		nTrigCount = this.nTriggerCount.incrementAndGet();
		//
		//		if (nTrigCount >= Node.MAX_TRIGGER) {
		//			StringBuilder stbResponse = new StringBuilder("nTrigCount:" + nTrigCount + " ");
		//			stbResponse.append(parentNodes.sendToChildren("addToCount"));
		//
		//			this.nTriggerCount.set(0);
		//			strReason = stbResponse.toString();
		//		}
		//		else {
		//			strReason = String.format("No trigger, count is:%d", nTrigCount);
		//		}
		//	}
		//	finally {
		//		lock.unlock();
		//	}

		return strReason;
	}
}
