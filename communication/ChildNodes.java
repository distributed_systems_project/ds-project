import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ChildNodes {

	private HashMap<Integer, InetSocketAddress> childrenIpAddress;
	private Lock lock = new ReentrantLock();
	public int nNodeNum;
	private int nSentMessages = 0;
	private boolean bSentToChildren = false;

	public ChildNodes (int nodeNum) {		
		childrenIpAddress = new HashMap<Integer, InetSocketAddress> ();
		this.nNodeNum = nodeNum;
		if (Symbols.LOGGING >= 3 )
			System.out.println("[ChildNodes.java]<" + this.nNodeNum + ">ChildNodes class instantiated");		
	}

	public void addChildNode (int nNodeNum, int nPort) {
		addChildNode(nNodeNum, "localhost", nPort);
	}

	public void addChildNode (int nNodeNum, String hostName, int nPort) {

		InetSocketAddress inetSock = new InetSocketAddress(hostName, nPort);		
		addChildNode(nNodeNum, inetSock);
	}

	public void addChildNode (int nNodeNum, InetSocketAddress inetSock) {
		try {
			lock.lock();
			childrenIpAddress.put(nNodeNum, inetSock);
			if (Symbols.LOGGING >= 3 )
				System.out.printf("[ChildNodes.java]<%d>Adding child node %d:%s\n", this.nNodeNum, nNodeNum, inetSock.toString());	
		}
		finally {
			lock.unlock();
		}
	}
	
	public int getSentMessages() {
		return nSentMessages;
	}
	
	public boolean getSentToChildren(){
		return this.bSentToChildren;
	}

	public void removeChildNode (int nNodeNum) {
		try {
			lock.lock();
			childrenIpAddress.remove(nNodeNum);
		}
		finally {
			lock.unlock();
		}
	}

	public void clearAllChildNode (int nNodeNum) {
		try {
			lock.lock();
			childrenIpAddress.clear();
		}
		finally {
			lock.unlock();
		}
	}

	public Set<Integer> getNodes () {
		return childrenIpAddress.keySet();
	}

	public Collection<InetSocketAddress> getValues () {
		return childrenIpAddress.values();
	}

	public boolean isLeaf() {
		boolean bRoot = true;
		if (!childrenIpAddress.isEmpty())
			bRoot = false;
		return bRoot;
	}
	
	public String sendToChildren(String command) {
		
		StringBuilder stbResponse = new StringBuilder();

		for ( InetSocketAddress address : childrenIpAddress.values()) {
			stbResponse.append("Notifying:" + address.toString() + " ");

			if (Symbols.LOGGING >= 3 )
				System.out.printf("[ChildNodes.java]<%d>In ChildNodes client:%s cmd:%s\n", this.nNodeNum, address.toString(), command);
			nSentMessages++;
			
			try (Socket tcpSocket = new Socket(address.getHostName(), address.getPort());
					PrintWriter tcpOut = new PrintWriter(tcpSocket.getOutputStream(), true);
					BufferedReader tcpIn = new BufferedReader(new InputStreamReader(tcpSocket.getInputStream()));
					)
					{
				// Send command to server
				tcpOut.println(command);
				if (Symbols.LOGGING >= 3 )
					System.out.printf("[ChildNodes.java]<%d>sendToChildren after tcpOut\n", this.nNodeNum);

				// Display response from server
				if (Symbols.LOGGING >= 3 )
					System.out.println("[ChildNodes.java]<"+ this.nNodeNum + ">" + "Not implemented");//tcpIn.readLine());
				tcpOut.close();
					}
			catch (IOException e) {
				if (Symbols.LOGGING >= 2 )
					e.printStackTrace();
			}
			finally {}
		}
		
		this.bSentToChildren = true;
		
		return stbResponse.toString();
	}
}
