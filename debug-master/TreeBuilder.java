import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class TreeBuilder {

	int nNumberOfNodes = 0;
	int nScaleFactor = 0;
	Map<Integer, Node> nodeTree;

	public TreeBuilder(int numNodes, int scaleFactor) {

		this.nNumberOfNodes = numNodes;
		this.nScaleFactor = scaleFactor;
	}
	
	public Map<Integer, Node> getNodeTree() {
		return nodeTree;
	}
	
	public Map<Integer, Node> buildTree() {
		int nodesInPreviousLevel = 0;
		int nodesInCurrentLevel = 1;
		int nodesInNextLevel = nodesInCurrentLevel * this.nScaleFactor;
		int nodesCreated = 0;
		int nodeLevel = 0;
		int parentStart = 0;
		int parentEnd = 0;
		int childStart = 0;
		int childEnd = 0;	
		int currentStart = 0;
		int currentEnd = 0;
		nodeTree = new HashMap<Integer, Node> ();
		Node tempNode;
		
		if (Symbols.LOGGING >= 3 )
			System.out.printf("Root node\n");
		nodesCreated++;
		tempNode = new Node(nodesCreated, nodeLevel);

		//setup of parameters
		currentStart = 1;
		currentEnd = 1;
		childStart = nodesCreated + 1 ;
		childEnd = (nodesCreated + nodesInNextLevel < nNumberOfNodes) ? nodesCreated + nodesInNextLevel: nNumberOfNodes;

		for (int j = childStart; j <= childEnd; j++) {
			if (Symbols.LOGGING >= 3 )
				System.out.printf("Child node:%d\n", j);
			tempNode.addChildNode(j);
		}
		nodeTree.put(nodesCreated, tempNode);
		
		while (nodesCreated < nNumberOfNodes) {		
			parentStart = currentStart;
			parentEnd = currentEnd;
			currentStart = childStart;
			currentEnd = childEnd;					
			childStart = currentEnd + 1;
			nodesInPreviousLevel = nodesInCurrentLevel;
			nodesInCurrentLevel = nodesInNextLevel;
			nodesInNextLevel *= this.nScaleFactor;
			childEnd = (childStart -1 + nodesInNextLevel < nNumberOfNodes) ? childStart - 1 + nodesInNextLevel: nNumberOfNodes;
			nodeLevel++;
			if (Symbols.LOGGING >= 3 )
				System.out.printf("parentStart:%d parentEnd:%d currentStart:%d currentEnd:%d childStart:%d childEnd:%d nodesInPreviousLevel:%d nodesInCurrentLevel:%d nodesInNextLevel:%d nodeLevel:%d\n", 
						parentStart, parentEnd, currentStart, currentEnd, childStart, childEnd, nodesInPreviousLevel, nodesInCurrentLevel, nodesInNextLevel, nodeLevel);

			
			for (int i = currentStart; i <= currentEnd; i++) {
				nodesCreated++;
				tempNode = new Node(nodesCreated, nodeLevel);
				
				if (Symbols.LOGGING >= 3 )
					System.out.printf("Level:%d Node#:%d\n", nodeLevel, nodesCreated);	
				for (int j = parentStart; j <= parentEnd; j++) {
					tempNode.addParentNode(j);
					
					if (Symbols.LOGGING >= 3 )
						System.out.printf("Parent node:%d%n", j);

				}
				for (int j = childStart; j <= childEnd; j++) {
					tempNode.addChildNode(j);

					if (Symbols.LOGGING >= 3 )
						System.out.printf("Child node:%d%n", j);
				}
				nodeTree.put(nodesCreated, tempNode);

			}
		}
		
		return nodeTree;
		
		
	}
	
	public static void main (String[] args) {
		TreeBuilder bTree = new TreeBuilder(7, 2);
		
		bTree.buildTree();		
	}

}
