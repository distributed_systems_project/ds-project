import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Collection;
import java.util.Map;

import static java.lang.Thread.sleep;


public class DebugMaster {
    public static void main(String[] args) {


        // Parse Command Line Arguments
        int N = Integer.parseInt(args[0]);
        int W = Integer.parseInt(args[1]);

		if (Symbols.LOGGING >= 1 ) {
			System.out.println("[Debug-Master] Debug Master...at your service!");
			System.out.printf("[Debug-Master] N=%d, W=%d%n", N, W);
		}

        Node node;
        TcAlgorithm alg;

        TreeBuilder bTree = new TreeBuilder(N, 2);
        Map<Integer, Node> nodes = bTree.buildTree();

        for(int i = 1; i <= bTree.nNumberOfNodes; i++)
        {
            node = nodes.get(i);
            alg = new TcLayeredNode(W, node.getNodeLevel(), node.getParentNodes(), node.getChildtNodes(), 0, bTree.nNumberOfNodes, i);
            node.addAlgorithm(alg);

        }

        // Send Start to Root Node
        sendStart(1);

        // Send W triggers
        int starting_node=4;
        int ending_node=N;
        int selected_node=starting_node-1;
        for(int k = 0; k < (W + W/2 ); k++) {

            selected_node++;
            if(selected_node > ending_node)
            {
                selected_node = starting_node;
            }
            sendTrigger(selected_node);

        }
		if (Symbols.LOGGING >= 3 )
			System.out.printf("[Debug-Master] finished sending triggers%n");
        
		try { sleep(3000); } catch(Exception e){}

        // Confirm all nodes have terminated
        for(int i = 1; i <=N; i++)
        {
           node = nodes.get(i);
            while( !((TcLayeredNode)node.getAlgorithm()).reportTerminated())
            {
        		
        			System.out.printf("[Debug-Master] waiting for Node %d to terminate. %n", i);
                try { sleep(1000); } catch(Exception e){}
            }
        }

			System.out.printf("[Debug-Master] all nodes have terminated%n");

        // Collect statistics
        int maxMsgs = 0;
        int maxMsgsAtNode = 0;

        for(int i = 1; i <=N; i++)
        {
            node = nodes.get(i);

            int msgs = Math.max(node.getRecvMessages(), node.getSentMessages());
            if(msgs > maxMsgs)
            {
                maxMsgs = msgs;
                maxMsgsAtNode = i;
            }
        }

			System.out.printf("[Debug-Master] Node %d sent or received max number of messages (%d messages)%n", maxMsgsAtNode, maxMsgs);

        int msgComplexity = 0;

        for(int i = 1; i <=N; i++)
        {
            node = nodes.get(i);

            int msgs = node.getSentMessages();
            msgComplexity += msgs;
        }

			System.out.printf("[Debug-Master] Message complexity of %d messages to count %d triggers to %d nodes%n", msgComplexity, W, N);

        for(int i = 1; i <=N; i++) {
            node = nodes.get(i);

            TcLayeredNode a = ((TcLayeredNode) node.getAlgorithm());

            System.out.printf("[Debug-Master] Node %d counted %d early, %d late, and %d total triggers%n", i, a.reportEarlyTriggers(), a.reportLateTriggers(), a.reportTotalTriggers());


        }


        try { sleep(1000); } catch(Exception e){}
    }


    public static void sendStart(int nodeid)
    {
        sendMsg(new InetSocketAddress("localhost", nodeid + Node.PORT_OFFSET), "start");
    }

    public static void sendTrigger(int nodeid)
    {
        sendMsg(new InetSocketAddress("localhost", nodeid + Node.PORT_OFFSET), "trigger");
        try { sleep(10); } catch(Exception e){}
    }

    public static void sendMsg(InetSocketAddress address, String command) {
		if (Symbols.LOGGING >= 3 )
			System.out.printf("[Debug-Master] sent to:%d cmd:%s\n", (address.getPort() - 10000), command);
        try (Socket tcpSocket = new Socket(address.getHostName(), address.getPort());
             PrintWriter tcpOut = new PrintWriter(tcpSocket.getOutputStream(), true);
             BufferedReader tcpIn = new BufferedReader(new InputStreamReader(tcpSocket.getInputStream()));
        )
        {
            // Send command to server
            tcpOut.println(command);

            // Display response from server
            //System.out.println("[Debug-Master] received:" + tcpIn.readLine());
        }
        catch (IOException e) {
    		if (Symbols.LOGGING >= 3 )
    			e.printStackTrace();
        }
        finally {}
    }
}