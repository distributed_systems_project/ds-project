

public class TcTrivialCentralizedMaster implements TcAlgorithm {
  
  private int threshold;
  private int totalCount;
  
  public TcTrivialCentralizedMaster(int threshold) {
    this.totalCount = 0;
    this.threshold = threshold;
  }
  
  public void receiveTrigger() {
    this.totalCount += 1;
  }
  
  private boolean checkCount() {
    return (this.threshold == this.totalCount);
  }
  
  
// When receiving message, add to count.
  
  
// Compare current count to threshold;
// Send termination message if threshold is reached.
  
  
}