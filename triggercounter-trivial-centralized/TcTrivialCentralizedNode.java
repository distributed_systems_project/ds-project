

public class TcTrivialCentralizedNode implements TcAlgorithm {
  
  private int messagesSent;
  private int myCount;

  public void increaseCount(int numberToAdd){};

  public TcTrivialCentralizedNode() {
    this.myCount = 0;
    this.messagesSent = 0;
  }
  
  public void receiveTrigger() {
    myCount += 1;
    sendCountToMaster();
    
    // Also send message to debugMaster with current count
  }
  
  private void sendCountToMaster() {
    // Message to Master should include the number of triggers received.
    // 
    this.messagesSent++;
  }

  
// When receiving termination message, stop.
  
  
}