public interface TcAlgorithm
{
    public void receiveMessage(String message);
    public boolean reportTerminated();
}